from nltk.tokenize import word_tokenize
import nltk
from nltk.corpus import stopwords
import sparql
import tags
import sys

search = input("Search:")
processedWords = []
words = nltk.word_tokenize(search)
sTwords = stopwords.words('english')

#Removing stopwords
for word in words:
    if word not in sTwords:
        processedWords.append(word)

tagged_words = nltk.pos_tag(processedWords)
sentence = ' '.join([word[0] for word in tagged_words])
#print(tagged_words)
print('Searching...')

def genre(words):
    words = [word for word in words
             if tags.NOUN_PROPER_SINGULAR in word[tags.TAG_INDEX] and \
             word[tags.WORD_INDEX] not in identifiers and \
             word[tags.WORD_INDEX] != 'music' and \
             word[tags.WORD_INDEX] != 'kind']
    words = [word[tags.WORD_INDEX] for word in words]
    if len(words) > 0:
        return sparql.genre(words)
    return None

if(tagged_words[0][1] == 'WP' or tagged_words[0][1] == 'WDT'):

    identifiers = [
        'genre',
        'kind of music',
        'kind of genre'
    ]
    for identifier in identifiers:
        if identifier in sentence:
            genre(tagged_words)

    exit()

#Case where was born
if (tagged_words[0][1] == 'WRB' and tagged_words[0][0] == 'Where' and tagged_words[1][1] == 'VBN'):
    term = ''
    i = 0
    for word in tagged_words:
        if (word[1] == 'NNP'):
            if (i == 0):
                term += word[0]
            else:
                term += ' ' + word[0]

            i += 1
    
    sparql.whereWasBorn(term)

#Case who
if (tagged_words[0][1] == 'WP' and tagged_words[0][0] == 'Who'):
    name = ''
    i = 0
    for word in tagged_words:
        if (word[1] == 'NNP'):
            if (i == 0):
                name += word[0]
            else:
                name += ' ' + word[0]
        
            i += 1
    sparql.whoIs(name)

#Case where
if (tagged_words[0][1] == 'WRB'):
    place = ''
    i = 0
    for word in tagged_words:
        if (word[1] != 'WRB' and word[1] != '.'):
            if (i == 0):
                place += word[0]
            else:
                place += ' ' + word[0]

            i += 1
            
    sparql.whereIs(place)
#case what birth name of artist
if (tagged_words[0][1] == 'WP' and tagged_words[0][0] == 'What' and tagged_words[1][0] == 'birth' and
        tagged_words[2][0] == 'name'):
    term = ''
    i = 0
    for word in tagged_words:
        if (word[1] == 'NNP'):
            if (i == 0):
                term += word[0]
            else:
                term += ' ' + word[0]

            i += 1
    sparql.birthName(term)
elif(tagged_words[0][1] == 'WP' and tagged_words[0][0] == 'What' and tagged_words[1][0] == 'birth' and
     (tagged_words[2][0] == 'place' or tagged_words[2][0] == 'city')):
    #case what is the birth place/city of artist
    term = ''
    i = 0
    for word in tagged_words:
        if (word[1] == 'NNP'):
            if (i == 0):
                term += word[0]
            else:
                term += ' ' + word[0]

            i += 1
    sparql.birthCity(term)

elif (tagged_words[0][1] == 'WP' and tagged_words[0][0] == 'What'):
#Case what
    term = ''
    i = 0
    for word in tagged_words:
        if (word[1] != 'WP' and word[1] != '.'):
            if (i == 0):
                term += word[0]
            else:
                term += ' ' + word[0]

            i += 1

    sparql.whatIs(term)

# case how to cook
if (tagged_words[0][1] == 'WRB' and tagged_words[0][0] == 'How'):
    term = ''
    i = 0
    for word in tagged_words:
        if (word[1] == 'NNP'):
            if (i == 0):
                term += word[0]
            else:
                term += ' ' + word[0]

            i += 1
    
    sparql.howToCook(term)





