from SPARQLWrapper import SPARQLWrapper, JSON
#quem é
def whoIs(person):
  sparql = SPARQLWrapper("http://dbpedia.org/sparql")
  sql = """ 
      PREFIX  dbpedia-owl:  <http://dbpedia.org/ontology/>
      PREFIX dbpedia: <http://dbpedia.org/resource>
      PREFIX dbpprop: <http://dbpedia.org/property>

     SELECT DISTINCT ?person ?comment ?label
      WHERE {
        ?person rdf:type dbpedia-owl:Person.
        ?person rdfs:comment ?comment.  
        ?person rdfs:label ?label
        FILTER regex(?label, "^%s", "i")
        FILTER (LANG(?comment) = 'pt') 
      }
      LIMIT 1
      """ % ''.join((person))
  
  sparql.setQuery(sql)
  sparql.setReturnFormat(JSON)
  results = sparql.query().convert()

  for result in results["results"]["bindings"]:
      print(result["comment"]["value"])

#onde é
def whereIs(location):
  sparql = SPARQLWrapper("http://dbpedia.org/sparql")
  sql = """ 
      PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
      PREFIX dbo: <http://dbpedia.org/ontology/>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX dbp: <http://dbpedia.org/property/>

      SELECT *
      WHERE  { 
        ?location rdf:type dbo:Location.
        ?location dbo:location ?country.
        ?location rdfs:label ?label.
        OPTIONAL {
          ?country dbp:coordinatesType ?city.
        }
        ?country rdfs:label ?countryLabel.

        FILTER regex(?label, "^%s", "i").    
      }
      LIMIT 1
      """ % ''.join((location))

  sparql.setQuery(sql)
  sparql.setReturnFormat(JSON)
  results = sparql.query().convert()

  for result in results["results"]["bindings"]:
      print(result["countryLabel"]["value"])

#o que é
def whatIs(term):
  sparql = SPARQLWrapper("http://dbpedia.org/sparql")
  sql = """ 
  PREFIX w3-owl: <http://www.w3.org/2002/07/owl#>
  SELECT ?thing, ?comment, ?label
    WHERE {
      ?thing rdf:type w3-owl:Thing.
      ?thing rdfs:comment ?comment.
      ?thing rdfs:label ?label.
      FILTER regex(?label, "^%s", "i").
      FILTER (lang(?comment) = 'pt')
    }
  LIMIT 1
  """ % ''.join((term))

  sparql.setQuery(sql)

  sparql.setReturnFormat(JSON)
  results = sparql.query().convert()

  for result in results["results"]["bindings"]:
      print(result["comment"]["value"])

#como cozinhar uma comida
def howToCook(term):
  sparql = SPARQLWrapper("http://dbpedia.org/sparql")
  sql = """ 
  PREFIX  dbpedia-owl:  <http://dbpedia.org/ontology/>
  PREFIX dbpedia: <http://dbpedia.org/resource>
  PREFIX dbpprop: <http://dbpedia.org/property>
     SELECT *
      WHERE {
        ?dessert rdf:type dbpedia-owl:Food.
        ?dessert dbpedia-owl:ingredientName ?ing.
        ?dessert dbpedia-owl:servingTemperature ?servingTemp.
        ?dessert rdfs:comment ?comment.
        ?dessert rdfs:label ?label.
        FILTER regex(?label, "^%s", "i")
        
        FILTER (LANG(?comment) = 'pt') 

      }
      LIMIT 1
  """ % ''.join((term))

  sparql.setQuery(sql)

  sparql.setReturnFormat(JSON)
  results = sparql.query().convert()

  for result in results["results"]["bindings"]:
      print(result["comment"]["value"] + "\n")
      print('Ingredientes: ' + result["ing"]["value"] + "\n")
      print('Servir: ' + result["servingTemp"]["value"] + "\n")

#onde uma pessoa nasceu
def whereWasBorn(person):
  sparql = SPARQLWrapper("http://dbpedia.org/sparql")
  sql = """ 
      PREFIX dbo: <http://dbpedia.org/ontology/>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

      SELECT *
      WHERE  { 
        ?person rdf:type dbo:Person.
        ?person rdfs:label ?label.
        ?person dbo:birthPlace ?country.
        ?country rdfs:label ?birthPlace.
        FILTER regex(?label, "^%s", "i")

      }

      LIMIT 1
      """ % ''.join((person))
  
  sparql.setQuery(sql)

  sparql.setReturnFormat(JSON)
  results = sparql.query().convert()

  for result in results["results"]["bindings"]:
      print(result["birthPlace"]["value"])

#nome de nascimento do artista
def birthName(artist):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sql = """ 
         PREFIX dbo: <http://dbpedia.org/ontology/>
         PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
         PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

         SELECT ?name
         WHERE  { 
           ?person rdf:type dbo:MusicalArtist ;
                   rdfs:label ?label ;
                   dbo:birthName ?name
           FILTER regex(?label, "^%s", "i")
         }
         LIMIT 1
         """ % ''.join(artist)
    sparql.setQuery(sql)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    print("The birth name is " + results["results"]["bindings"][0]["name"]["value"])

#cidade de nascimento do artista
def birthCity(artist):
    wrapper = SPARQLWrapper("http://dbpedia.org/sparql")
    wrapper.setReturnFormat(JSON)
    sql = """
            PREFIX dbo: <http://dbpedia.org/ontology/>
          SELECT ?city_name WHERE
          {
          ?song dbo:musicalArtist ?artist.
          ?artist dbo:birthPlace ?city ;
                  rdfs:label ?label.
            ?city rdfs:label ?city_name
           FILTER regex(?label, "^%s", "i")
          }
            LIMIT 1
          """ % ''.join(artist)
    wrapper.setQuery(sql)

    wrapper.setReturnFormat(JSON)
    result = wrapper.query().convert()
    print('The city is ' + result['results']['bindings'][0]["city_name"]["value"])

def genre(artist):
    wrapper = SPARQLWrapper("http://dbpedia.org/sparql")
    wrapper.setReturnFormat(JSON)
    wrapper.setQuery(
        """ 
        PREFIX dbpedia-owl:  <http://dbpedia.org/ontology/>
        
        SELECT DISTINCT ?label WHERE {
            ?thing foaf:name ?name ;
            foaf:isPrimaryTopicOf ?url .
            ?name  bif:contains "'%s'" .
            {
                ?thing dbpedia-owl:genre ?genre ;
                a                 dbpedia-owl:Band
            }
            UNION
            {
                ?thing dbpedia-owl:genre ?genre ;
                a                 dbpedia-owl:MusicalArtist
            }
            UNION
            {
                ?thing a <http://umbel.org/umbel/rc/MusicalPerformer>
            }
        
            ?genre rdfs:label ?label
            FILTER (LANG(?label) = 'en') 
        }
        LIMIT 5
        """ % ' '.join(artist)
    )
    results = wrapper.query().convert()['results']['bindings']
    if len(results) > 0:
        genres = []
        for result in results:
            genres.append(result['label']['value'])
        answer = ' '.join(artist) + " plays " + ', '.join(genres)
        print(answer)
    else:
        print("No results found")