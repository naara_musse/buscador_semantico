# Info from:
# https://stackoverflow.com/questions/15388831/what-are-all-possible-pos-tags-of-nltk

WH_ADVERB = 'WRB'
# how however whence whenever where whereby wherever wherein whereof why
WH_PRONOUN = 'WP'
# that what whatever whatsoever which who whom whosoever
WH_DETERMINER = 'WDT'
# that what whatever which whichever
NOUN_PROPER_SINGULAR = 'NNP'
# Motown Venneboerger Czestochwa Ranzer Conchita Trumplane Christos
# Oceanside Escobar Kreisler Sawyer Cougar Yvette Ervin ODI Darryl CTCA
# Shannon A.K.C. Meltex Liverpool ...
NOUN = 'NN'
# common-carrier cabbage knuckle-duster Casino afghan shed thermostat
# investment slide humour falloff slick wind hyena override subhumanity
# machinist ...


TAG_INDEX = 1
WORD_INDEX = 0